import pyglet

window = pyglet.window.Window()
label = pyglet.text.Label('Hello, world',
                          font_name="Calibri",
                          font_size=36,
                          x=window.width//2, y=window.height//2,
                          anchor_x='center', anchor_y='center')

image = pyglet.resource.image('example.png')

music = pyglet.resource.media('13 Hysteria.wma')
music.play()

'''
@window.event
def on_draw():
    window.clear()
    label.draw()
'''

@window.event
def on_draw():
    window.clear()
    image.blit(0, window.height//2)

pyglet.app.run()