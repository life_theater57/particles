# circle.py
#
# draws a circle, where the points for the vertex list are computed at run-time

from math import *
from pyglet.gl import *

window = pyglet.window.Window()
'''
labelXTag = pyglet.text.Label('x:',
                              font_name="Calibri",
                              font_size=20,
                              x=10, y=window.height-30,
                              anchor_x='center', anchor_y='center')
labelYTag = pyglet.text.Label('y:',
                              font_name="Calibri",
                              font_size=20,
                              x=10, y=window.height-60,
                              anchor_x='center', anchor_y='center')
labelXValue = pyglet.text.Label('0',
                                font_name="Calibri",
                                font_size=20,
                                x=40, y=window.height-30,
                                anchor_x='center', anchor_y='center')
labelYValue = pyglet.text.Label('0',
                                font_name="Calibri",
                                font_size=20,
                                x=40, y=window.height-60,
                                anchor_x='center', anchor_y='center')
'''

def createCircle(center_x, center_y, radius, numPoints):
    verts = []
    angle = 360 / numPoints
    for i in range(numPoints):
        radAngle = (angle * i) * (pi / 180)
        sinTheta = sin(radAngle)
        cosTheta = cos(radAngle)
        x = center_x + (cosTheta - sinTheta) * radius
        y = center_y + (sinTheta + cosTheta) * radius
        verts += [x,y]
    return pyglet.graphics.vertex_list(numPoints, ('v2f', verts))

circle = createCircle(window.width // 2, window.height // 2, 75, 50)
circle2 = createCircle(window.width // 2, window.height // 2, 50, 50)

@window.event
def on_mouse_motion(x, y, dx, dy):
    circle2 = createCircle(x, y, 50, 50)
    #labelXValue.text = str(x)
    #labelYValue.text = str(y)

@window.event
def on_draw():
    glClear(pyglet.gl.GL_COLOR_BUFFER_BIT)
    #labelXTag.draw()
    #labelYTag.draw()
    #labelXValue.draw()
    #labelYValue.draw()
    glColor3f(0,0.6,0)
    circle.draw(GL_TRIANGLE_FAN)
    glColor3f(1,1,0)
    circle2.draw(GL_TRIANGLE_FAN)
    circle2.delete()

pyglet.app.run()