from math import *
from pyglet.gl import *
from pyglet import clock
import numpy as np

class SimObject():
    # Static variables
    max_index = 100
    next_index = 0
    positions = np.zeros((max_index,3))
    velocities = np.zeros((max_index,3))
    epsilon = 0.5
    objectSet = set()
    
    def __init__(self, position = np.array([-1,-1,-1]), velocity = np.zeros(3), mass = 0, radius = 0):
        self.mass = mass
        self.radius = radius
        self.index = SimObject.next_index
        SimObject.next_index = SimObject.next_index + 1
        SimObject.positions[self.index] = position
        SimObject.velocities[self.index] = velocity
        SimObject.objectSet.add(self)
        

    def position(self, value = None):
        if value is None:
            return SimObject.positions[self.index]
        else:
            SimObject.positions[self.index] = value
    
    def velocity(self, value = None):
        if value is None:
            return SimObject.velocities[self.index]
        else:
            SimObject.velocities[self.index] = value
    
    def checkFieldBounds(self, dt):
        (conflict_dim, conflict_dt) = self.checkFieldBounds_helper(dt)
        if conflict_dt > 0:
            self.position(self.position() - conflict_dt * self.velocity())
            v = self.velocity()
            v[conflict_dim] = -v[conflict_dim]
            self.position(self.position() + conflict_dt * v)
            self.velocity(v)
            self.checkFieldBounds(conflict_dt)
    
    def checkFieldBounds_helper(self, dt):
        p = self.position()
        dim = -1
        max_conflict_dt = 0
        for d in range(len(dimensions)):
            conflict_dt = 0
            
            if p[d] - self.radius < 0:
                conflict_dt = (p[d] - self.radius) / self.velocity()[d]
            elif p[d] + self.radius > dimensions[d]:
                conflict_dt = (p[d] + self.radius - dimensions[d]) / self.velocity()[d]
            
            if conflict_dt > max_conflict_dt:
                max_conflict_dt = conflict_dt
                dim = d

        return (dim, max_conflict_dt)
                
    
    @staticmethod
    def update(dt):
        SimObject.positions = SimObject.positions + SimObject.velocities * dt
        remainingObjects = SimObject.objectSet
        for obj1 in SimObject.objectSet:
            #TODO: list all object currently in conflict, because moving an object can put it in conflict with another
            #remainingObjects.remove(obj1)
            for obj2 in remainingObjects:
                if (obj1.index != obj2.index) and (((obj1.position() - obj2.position()) ** 2).sum() < ((obj1.radius + obj2.radius) ** 2)): 
                    p1 = obj1.position() - dt * obj1.velocity()
                    p2 = obj2.position() - dt * obj2.velocity()
                    limits = np.array([0, dt])
                    target = (obj1.radius + obj2.radius) ** 2
                    last_dt = limits.mean()
                    diff = ((p1 - p2 + last_dt * (obj1.velocity() - obj2.velocity())) ** 2).sum()
                    # Descente de gradient
                    while abs(diff - target) > SimObject.epsilon and limits[1] - limits[0] > 1e-6:
                        limits[int(diff < target)] = last_dt
                        last_dt = limits.mean()
                        diff = ((p1 - p2 + last_dt * (obj1.velocity() - obj2.velocity())) ** 2).sum()

                    v1=(((obj1.mass-obj2.mass)*obj1.velocity()+2*obj2.mass*obj2.velocity())/(obj1.mass+obj2.mass))
                    v2=(((obj2.mass-obj1.mass)*obj2.velocity()+2*obj1.mass*obj1.velocity())/(obj1.mass+obj2.mass))
                    
                    obj1.position(p1 + obj1.velocity() * last_dt)
                    obj1.position(obj1.position() + v1 * (dt - last_dt))
                    obj1.velocity(v1)
                    
                    obj2.position(p2 + obj2.velocity() * last_dt)
                    obj2.position(obj2.position() + v2 * (dt - last_dt))
                    obj2.velocity(v2)
            obj1.checkFieldBounds(dt)
            

    @staticmethod
    def size():
        return SimObject.next_index

class Circle():
    # Static variables
    numPoints = 50    

    def __init__(self, radius, color):
        self.radius = radius
        self.color = color
        self.dynamic = None
    
    def draw(self, mode):
        glColor3f(self.color[0],self.color[1],self.color[2])
        center = self.position()
        verts = []
        angle = 360 / Circle.numPoints
        for i in range(Circle.numPoints):
            radAngle = (angle * i) * (pi / 180)
            sinTheta = sin(radAngle)
            cosTheta = cos(radAngle)
            x = center[0] + (cosTheta - sinTheta) * self.radius
            y = center[1] + (sinTheta + cosTheta) * self.radius
            verts += [x,y]
        self.shape = pyglet.graphics.vertex_list(Circle.numPoints, ('v2f', verts))
        self.shape.draw(mode)
        self.shape.delete()
    
    def setDynamic(self, simObject):
        self.dynamic = simObject

    def position(self):
        return self.dynamic.position()

dimensions = (1000, 600)
window = pyglet.window.Window(dimensions[0], dimensions[1], resizable=True)
fps_display = pyglet.clock.ClockDisplay()

circle1 = Circle(12.5, np.array([0,0.6,0]))
circle1.setDynamic(SimObject(np.array([100, window.height // 2, 0]), np.array([20, 20, 0]), 1, 12.5))
circle2 = Circle(12.5, np.array([1,1,0]))
circle2.setDynamic(SimObject(np.array([800, window.height // 2, 0]), np.array([-60, 20, 0]), 10, 12.5))

labelPos1 = pyglet.text.Label('pos ' + str(circle1.dynamic.index) + ':',
                              font_name="Calibri",
                              font_size=20,
                              x=10, y=window.height-30,
                              anchor_x='left', anchor_y='center')
labelPos2 = pyglet.text.Label('pos ' + str(circle2.dynamic.index) + ':',
                              font_name="Calibri",
                              font_size=20,
                              x=10, y=window.height-60,
                              anchor_x='left', anchor_y='center')
labelPos1Value = pyglet.text.Label('0',
                                font_name="Calibri",
                                font_size=20,
                                x=80, y=window.height-30,
                                anchor_x='left', anchor_y='center')
labelPos2Value = pyglet.text.Label('0',
                                font_name="Calibri",
                                font_size=20,
                                x=80, y=window.height-60,
                                anchor_x='left', anchor_y='center')


@window.event
def on_resize(width, height):
    glViewport(0, 0, width, height)
    glMatrixMode(gl.GL_PROJECTION)
    glLoadIdentity()
    glOrtho(0, width, 0, height, -1, 1)
    glMatrixMode(gl.GL_MODELVIEW)
    global dimensions
    dimensions = (width, height)
    
@window.event
def on_draw():
    # Clear surface
    glClear(pyglet.gl.GL_COLOR_BUFFER_BIT)
    # Display fps
    fps_display.draw()
    '''
    #
    labelPos1.draw()
    p = circle1.position()
    labelPos1Value.text = "(" + str(p[0]) + "," + str(p[1]) + ")"
    labelPos1Value.draw()
    #
    labelPos2.draw()
    p = circle2.position()
    labelPos2Value.text = "(" + str(p[0]) + "," + str(p[1]) + ")"
    labelPos2Value.draw()
    '''
    # Draw vertices
    c = circle1.color
    circle1.draw(GL_TRIANGLE_FAN)
    c = circle2.color
    circle2.draw(GL_TRIANGLE_FAN)


clock.schedule_interval(SimObject.update, 1/60)
pyglet.app.run()
'''
#donées de base de la balle 1
b1 = sphere (pos=vector (-5,0,0), radius=0.5, color=color.red)
v1=vector(10,0,0)
m1=1

#données de base de la balle 2
b2 = sphere (pos=vector (5,0,0), radius=0.5, color=color.magenta)
m2=10
v2= vector(0,0,0)

#appuyer sur espace pour lancer
print("appuyer sur espace pour lancer")
scene.waitfor('keydown')
print("appuyer sur espace pour pause")

#Afficher les vitesses initiales
print(v1,v2)

#distance entre les deux centres des balles (2xrayons)
d12=1

#gérer le temps
dt = 0.002
t=0

#créer la collision
while t<5:
    v1_i = v1
    v2_i = v2
    b1.pos=b1.pos+v1*dt
    b2.pos=b2.pos+v2*dt
    if b1.pos.x>b2.pos.x-d12: 
        v1=(((m1-m2)*v1_i+2*m2*v2_i)/(m1+m2))
        v2=(((m2-m1)*v2_i+2*m1*v1_i)/(m1+m2))
        b1.pos.x=b1.pos.x+v1.x*dt
        b2.pos.x=b2.pos.x+v2.x*dt
        print(v1,v2)

    t=t+dt
'''